/**
plantuml json 转 对象图
1 plantuml 支持直接渲染json，但是不支持里面引用同一个对象
2 idea的plantuml插件不支持渲染json
*/
/**返回一个id生成器*/
function idGenerator(){
    let id = 1
    return ()=>id++
}
function isString(o){
    return typeof o === 'string'
}
function isPrimitive(o){
    let type = typeof o
    return ['string','boolean','number'].includes(type)
}
function type(o){
    return Array.isArray(o)?'array':typeof o
}
/**遍历对象属性、数组元素。fn：(属性名/下标,属性值,对象/数组)=>undefined*/
function each(o,fn){
    if(isPrimitive(o)){
    }else if(Array.isArray(o)){
        for(let i=0;i<o.length;i++){
            fn(i,o[i],o)
        }
    }else{
        for(let k in o){
            fn(k,o[k],o)
        }
    }
}
/**
将数据扁平化。为对象/数组添加唯一标识（添加'@id'属性）。
返回对象Map和引用Map。
对象Map:key为对象id，value为对象。
引用Map:key为对象id，value为数组。数组每个元素是（对象/数组,属性/下标）的二元组。
将引用替换成'@ref'+被引用的对象/数组的id。
例如obj={msg:["hello"]}，假如为obj生成的id是1，为["hello"]这个数组生成的id是2，那么将会返回
{
  objs:{
    1:{
        @id:1,
        msg:'@ref2'
    },
    2:{
        @id:2,
        0:"hello"
    }
  },
  refs:{
        2:[[obj,'msg']]
  }
}
*/
function flatten(o){
    const nextId = idGenerator()
    const objs = {}
    const refs = {}
    doFlatten(o)
    return {objs,refs}
    function doFlatten(o){
        if(o['@id']){
            return
        }
        o['@id'] = nextId()
        objs[o['@id']] = o
        each(o,(k,v,p)=>{
            let isRef = false
            if(v['@id']){
                isRef = true
            }else if(typeof v ==='object'){
                isRef = true
                doFlatten(v)
            }
            if(isRef){
                p[k] = '@ref'+v['@id']
                if(!refs[v['@id']]){
                    refs[v['@id']] = []
                }
                refs[v['@id']].push([p,k])
            }
        })
    }
}
/**
根据js对象生成plantuml的对象图代码。
有时候对象图的根对象不止一个，可以在生成代码后自己再调整。
*/
function graph(o){
    let {objs,refs} = flatten(o)
    let res = ['@startuml']
    each(objs,(id,obj)=>{
        res.push('map '+type(obj)+id+'{')
        each(obj,(name,value)=>{
            if(name==='@id')return
            if(typeof value === 'string' && value.substr(0,4)==='@ref'){
                res.push('    '+name+' => ')
            }else{
                res.push('    '+name+' => '+value)
            }
        })
        res.push('}')
    });
    each(refs,(id,refArr)=>{
        refArr.forEach(ref=>{
            let [obj,key] = ref
            res.push(type(obj)+obj['@id']+'::'+key+' --> '+type(objs[id])+id)
        })
    })
    res.push('@enduml')
    return res.join('\n')
}

let obj = {
    oldOne:[[['a1','a2','a3','a4','a5','a6','a7','a8','a9','a10'],['a11','a12','x']]]
}
obj['newOne'] = [[obj.oldOne[0][0],['a11','a12','y']]]


console.info(graph(obj))


