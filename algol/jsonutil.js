//代码从MDN拷贝，修改了一下，添加对循环引用的支持
//https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/JSON
let JSONUtil = {
    //resolveCircular参数是我们加上去支持循环引用的
    parse: function(jsonStr,resolveCircular=true) {
        let res = eval('(' + jsonStr + ')')
        let cache = []
        let tasks = []
        let toString = Object.prototype.toString
        if(resolveCircular){
            resolveCir(res)
            tasks.forEach(args=>{
                let [obj,key] = args
                obj[key] = cache[obj[key].substr(4)]
            })
        }
        return res
        function resolveCir(obj){
            if(obj==null || !obj['@id'])return
            cache[obj['@id']] = obj
            delete obj['@id']
            for(let key in obj){
                if(typeof obj[key] === 'string' && obj[key].substr(0,4)==='@ref'){
                    tasks.push([obj,key])
                }else{
                    resolveCir(obj[key])
                }
            }
        }
    },
    stringify: (function () {
        let id = 1
        let cache = []
        let toString = Object.prototype.toString;
        let isArray = Array.isArray || function (a) { return toString.call(a) === '[object Array]'; };
        let escMap = {'"': '\\"', '\\': '\\\\', '\b': '\\b', '\f': '\\f', '\n': '\\n', '\r': '\\r', '\t': '\\t'};
        let escFunc = function (m) { return escMap[m] || '\\u' + (m.charCodeAt(0) + 0x10000).toString(16).substr(1); };
        let escRE = /[\\"\u0000-\u001F\u2028\u2029]/g;
        return function stringify(value,supportCircular=true) {
            if (value == null) {
              return 'null';
            } else if (typeof value === 'number') {
              return isFinite(value) ? value.toString() : 'null';
            } else if (typeof value === 'boolean') {
              return value.toString();
            } else if (typeof value === 'object') {
              if (typeof value.toJSON === 'function') {
                return stringify(value.toJSON());
              } else if (isArray(value)) {
                var res = '[';
                for (var i = 0; i < value.length; i++)
                  res += (i ? ', ' : '') + stringify(value[i]);
                return res + ']';
              } else if (toString.call(value) === '[object Object]') {
                if(supportCircular){
                    if(value['@id']){
                        return '"@ref'+value['@id']+'"'
                    }
                    if(value['@id']==null){
                        value['@id'] = id++
                        cache[id] = value
                    }
                }
                var tmp = [];
                for (var k in value) {
                  if (value.hasOwnProperty(k))
                    tmp.push(stringify(k) + ': ' + stringify(value[k]));
                }
                return '{' + tmp.join(', ') + '}';
              }
            }
            return '"' + value.toString().replace(escRE, escFunc) + '"';
        };
    })()
};